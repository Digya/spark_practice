package spark;

import org.apache.log4j.Level;
import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.log4j.Logger;

public class SparkProgrammingGuideDoc {

	public static void main(String[] args){

		class GetTotalLength implements Function2<Integer, Integer, Integer>{
			public Integer call(Integer x, Integer y){
				return x+y;
			}
		}

		Logger.getLogger("org").setLevel(Level.ERROR);
		SparkConf conf = new SparkConf().setAppName("SparkProgrammingGuideDoc").setMaster("local[*]");
		JavaSparkContext sc = new JavaSparkContext(conf);

		JavaRDD<String> textFile = sc.textFile("resources/newResource");
		JavaRDD<Integer> count = textFile.map(
				new Function<String, Integer>(){
					public Integer call(String x){
						return x.length();
					}
				});
		int totalCount = count.reduce(new GetTotalLength());

		System.out.println(totalCount);
		
		Accumulator<Integer> acc = sc.accumulator(0);
	}
}

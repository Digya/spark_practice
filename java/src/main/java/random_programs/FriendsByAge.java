package javaimpl;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

public class FriendsByAgeJavaImpl {

	public static void main(String[] args){
		class ReduceByKey implements Function2<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>, 
		Tuple2<Integer, Integer>>{

			@Override
			public Tuple2<Integer, Integer> call(Tuple2<Integer, Integer> x, 
					Tuple2<Integer, Integer>y) throws Exception {
				Tuple2<Integer, Integer> v = new Tuple2(x._1+y._1, x._2+y._2);
				return v;
			}	
		}

		class GiveAverage implements Function<Tuple2<Integer, Integer>,Double>{

			@Override
			public Double call(Tuple2<Integer, Integer> v1) throws Exception {
				double x = v1._1()/v1._2();
				return x;
			}

		}

		class Parse implements PairFunction<String, Integer, Integer>{

			@Override
			public Tuple2<Integer, Integer> call(String line) throws Exception {
				String[] splitted = line.split(",");
				Integer age = Integer.parseInt(splitted[2]);
				Integer numFriends = Integer.parseInt(splitted[3]);
				Tuple2<Integer, Integer> toReturn = new Tuple2<Integer, Integer>(age, numFriends);
				return toReturn;
			}

		}

		Logger.getLogger("org").setLevel(Level.ERROR);

		SparkConf conf = new SparkConf().setAppName("FriendsByAge.java").setMaster("local[*]");
		JavaSparkContext sc = new JavaSparkContext(conf);
		JavaRDD<String> file = sc.textFile("resources/fakefriends.csv");
		JavaPairRDD<Integer, Integer> tupled = file.mapToPair(new Parse());
		JavaPairRDD<Integer, Tuple2<Integer, Integer>>mapped = tupled.mapValues(
				new Function<Integer,Tuple2<Integer, Integer>>(){

					public Tuple2<Integer, Integer> call(Integer t) throws Exception {
						Tuple2<Integer, Integer> newTuple = new Tuple2(t,1);
						return newTuple;
					}
					
			});
		JavaPairRDD<Integer, Tuple2<Integer, Integer>> afterReduce = mapped.reduceByKey(new ReduceByKey());
		JavaPairRDD<Integer, Double> average = afterReduce.mapValues(new GiveAverage()).sortByKey();
		for(Tuple2<Integer, Double> a : average.collect()){
			System.out.println(a);
		}
	}
}

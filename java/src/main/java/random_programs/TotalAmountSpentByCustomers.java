package javaimpl;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

public class TotalAmountSpentByCustomersImpl {

	public static void main(String[] args){
		
		class parse implements PairFunction<String, Integer, Float>{
			@Override
			public Tuple2<Integer, Float> call(String line){
				String[] file = line.split(",");
				Integer age = Integer.valueOf(file[0]);
				Float numFriends = Float.valueOf(file[2]);
				return new Tuple2<Integer, Float>(age, numFriends);
			}	
		}
		
		Logger.getLogger("org").setLevel(Level.ERROR);
		SparkConf conf = new SparkConf().setAppName("T").setMaster("local[*]");
		JavaSparkContext sc = new JavaSparkContext(conf);
		
		JavaRDD<String> file = sc.textFile("customer-orders.csv");
		JavaPairRDD<Integer, Float> parsed = file.mapToPair(new parse());
		JavaPairRDD<Float, Integer> reduced = parsed.reduceByKey(
				new Function2<Float, Float, Float>(){

					@Override
					public Float call(Float v1, Float v2) 
							throws Exception {
						float v = (float)v1 + v2;
						return v;
					}
					
				}).mapToPair(
						new PairFunction<Tuple2<Integer, Float>, Float,Integer>(){

							@Override
							public Tuple2<Float, Integer> call(Tuple2<Integer, Float> v1) throws Exception {
								Tuple2<Float, Integer> x = new Tuple2<Float, Integer>(v1._2,v1._1);
								return x;
							}
							
						}).sortByKey();
		
		for(Object x : reduced.collect()){
			System.out.println(x);
		}
		
		
	}
}

package javaimpl;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.SparkContext.*;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;
import scala.Tuple3;

public class MinTemperaturesJavaImpl {                                     

	public static void main(String[] args){
		
		Logger.getLogger("org").setLevel(Level.ERROR);

		class Parse implements Function<String,Tuple3<String,String,Float>>{

			public Tuple3<String, String, Float> call(String line){
				String[] fields = line.split(",");
				String stationId = fields[0].toString();
				String entryType = fields[2].toString();
				Float temp = Float.valueOf(fields[3])* 0.1f * (9.0f / 5.0f) + 32.0f;
				Tuple3<String, String, Float> triple = new Tuple3(stationId, entryType, temp);
				return triple;

			}

			class ReducedByKey implements Function2<Float, Float, Float>{

				public Float call(Float x, Float y){
					return x+y;
				}
			}
		}
		Logger.getLogger("org").setLevel(Level.ERROR);

		SparkConf conf = new SparkConf().setAppName("MinTemperatures").setMaster("local[*]");
		JavaSparkContext sc = new JavaSparkContext(conf);

		JavaRDD<String> file = sc.textFile("resources/1800.csv");
		JavaRDD<Tuple3<String, String, Float>> parsed = file.map(new Parse());

		JavaPairRDD<String, Float> minimumTemp = parsed.filter(
				new Function<Tuple3<String, String, Float>, Boolean>(){

					@Override
					public Boolean call(Tuple3<String, String, Float> v1) 
							throws Exception {
						return v1._2().equals("TMIN");	
					}
				}).mapToPair(
						new PairFunction<Tuple3<String, String, Float>, String, Float>(){

							public Tuple2<String, Float> call(Tuple3<String, String, Float> t1) throws
							Exception{
								return new Tuple2(t1._1(), t1._3());
							}
						});

		JavaPairRDD<String, Float> mini = minimumTemp.reduceByKey(
				new Function2<Float, Float, Float>(){
					@Override
					public Float call(Float x, Float y){
						return Math.min(x, y);

					}
				}).sortByKey();
		for(Tuple2<String, Float> x : mini.collect()){
			System.out.println(x);
		}
	}
}

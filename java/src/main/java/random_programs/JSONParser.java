package javaimpl;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;

import parquet.org.codehaus.jackson.map.ObjectMapper;

public class JSONParser {

	public static void main(String[] args){
		
		Logger.getLogger("org").setLevel(Level.ERROR);
		SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("JSONParser");
		JavaSparkContext sc = new JavaSparkContext(conf);
		
		class ParseJSON implements FlatMapFunction<Iterator<String>, Person> {
			public Iterable<Person> call(Iterator<String> lines) throws Exception{
				ArrayList<Person> people = new ArrayList<Person>();
				ObjectMapper mapper = new ObjectMapper();
				while(lines.hasNext()){
					String line = lines.next();
					System.out.println(line);
					try{
						people.add(mapper.readValue(line, Person.class));
					} catch(Exception e){
						
					}
				}
				System.out.println("this is people" + people);
				return people;
			}
		}
		
		JavaRDD<String> file = sc.textFile("resources/file.json");
		JavaRDD<Person> result = file.mapPartitions(new ParseJSON());
		System.out.println(result);
		for(Person r : result.collect()){
			System.out.println(r.name + "  " + r.lovePandas);
		}
	}
}

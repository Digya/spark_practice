package javaimpl;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

import java.util.Arrays;

import org.apache.log4j.*;

public class QuickStart {

	public static void main(String[] args){

		Logger.getLogger("org").setLevel(Level.ERROR);

		SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("QuickStart");
		JavaSparkContext sc = new JavaSparkContext(conf);

		JavaRDD<String> file = sc.textFile("/home/lt27/spark-2.1.1-bin-hadoop2.7/README.md").cache();
		long count = file.count();
		System.out.println(count);

		String firstLine = file.first();
		System.out.println(firstLine);

		JavaPairRDD<String, Integer> wordCount = file.flatMap(
				new FlatMapFunction<String, String>(){
					public Iterable<String> call(String x){
						String[] y = x.split(" ");
						return Arrays.asList(y);
					}
				}).mapToPair(
						new PairFunction<String, String, Integer>(){
							public Tuple2<String, Integer> call(String x){
								return new Tuple2<String, Integer> (x,1);
							}
						}).reduceByKey(
								new Function2<Integer, Integer, Integer>(){
									public Integer call(Integer x, Integer y){
										Integer z = x+y;
										return z;
									}
								});

		System.out.println("the wordcount output is");
		for(Object o : wordCount.sortByKey(false).collect()){
			System.out.println(o);
		}

		Integer sizeLines = file.map(
				new Function<String, Integer>(){

					public Integer call(String x){
						int b = Arrays.asList(x.split(" ")).size();
						return b;
					}
				}).reduce(
						new Function2<Integer, Integer, Integer>(){
							public Integer call(Integer x, Integer y){
								if(x > y){
									return x;
								} else {
									return y;
								}
							}
						});

		System.out.println(sizeLines);
		Integer sizeLinesNext = file.map(
				new Function<String, Integer>(){
					public Integer call(String x){
						return Arrays.asList(x.split(" ")).size();
					}
				}).reduce(
						new Function2<Integer, Integer, Integer>(){
							public Integer call(Integer x, Integer y){
								return Math.max(x, y);
							}
						});

		System.out.println(sizeLinesNext);

		long numA = file.filter(
				new Function<String, Boolean>(){
					public Boolean call(String x){
						return x.contains("a");
					}
				}).count();
		
		long numB = file.filter(
				new Function<String, Boolean>(){
					public Boolean call(String x){
						return x.contains("b");
					}
				}).count();
		
		System.out.println("numbers A " + numA + " numbers B " + numB );

	}
}


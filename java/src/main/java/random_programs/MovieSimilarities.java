package javaimpl;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;
import scala.Tuple3;

import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;

public class MovieSimilaritiesImplJava {


	public static void main(String[] args){
		Logger.getLogger("org").setLevel(Level.ERROR);

		SparkConf conf = new SparkConf().setAppName("MovieSimilarities").setMaster("local[*]");
		JavaSparkContext sc = new JavaSparkContext(conf);

		JavaRDD<String> file = sc.textFile("resources/u.data");
		
		JavaRDD<Tuple2<Integer, Tuple2<Integer, Double>>> filee = file.map(
				new Function<String, String[]>(){
					public String[] call(String x){
						return x.split("\t");
					}
				}).map(
						new Function<String[], Tuple2<Integer, Tuple2<Integer, Double>>>(){
							public Tuple2<Integer, Tuple2<Integer,Double>> call(String[] x){
								Tuple2<Integer, Double> insideTuple = new Tuple2<Integer, Double>(Integer.valueOf(x[1]),
										Double.valueOf(x[2]));
								return new Tuple2<Integer, Tuple2<Integer, Double>>(Integer.valueOf(x[0])
										, insideTuple);
							}
						});
		
		for(Object o : filee.collect()){
			System.out.println(o);
		}
	
	
	}
}

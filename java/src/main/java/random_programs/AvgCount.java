package javaimpl;

import java.io.Serializable;

public class AvgCount implements Serializable{


	public AvgCount(int total, int count){
		this.total = total;
		this.count = count;
	}
	public int total;
	public int count;

	public double giveAverage(){
		return total/(double)count;
	}
}

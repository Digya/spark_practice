package javaimpl;

import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;

public class RatingsCounter {

	public static void main(String[] args){
		Logger.getLogger("org").setLevel(Level.ERROR);

		SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("T");
		JavaSparkContext sc = new JavaSparkContext(conf);

		JavaRDD<String> tFile = sc.textFile("../u.data");
		JavaRDD<Integer> value = tFile.map(
				new Function<String, Integer>(){
					public Integer call(String x){
						return Integer.parseInt(x.split("\t")[2]);
					}
				});
		Map<Integer, Long> answer = value.countByValue();
		System.out.println(answer);
	}
}

package javaimpl;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.*;


public class WordCountImplJava {

	public static void main(String[] args){
		
		Logger.getLogger("org").setLevel(Level.ERROR);
		
		SparkConf conf = new SparkConf().setAppName("WordCountImplJava").setMaster("local[*]");
		JavaSparkContext sc = new JavaSparkContext(conf);
		
		JavaRDD<String> file = sc.textFile("resources/book.txt");
		JavaPairRDD<String,Integer> output = file.flatMap(
				new FlatMapFunction<String, String>(){
					public Iterable<String> call(String x){
						return Arrays.asList(x.toLowerCase().split("\\W+"));
					}
				}).mapToPair(
						new PairFunction<String, String, Integer>(){

							@Override
							public Tuple2<String, Integer> call(String t) throws Exception {
								Tuple2<String, Integer> x = new Tuple2<String, Integer>(t,1);
								return x;
							}
							
						}).reduceByKey(
								new Function2<Integer, Integer, Integer>(){
									public Integer call(Integer x, Integer y){
										Integer out = x+y;
										return out;
									}
								}).sortByKey();
		
		List<Tuple2<String, Integer>> outputt = output.collect();
		for(Object x : outputt){
			System.out.println(x);
		}
		
	}
}

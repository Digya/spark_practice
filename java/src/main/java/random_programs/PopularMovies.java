package javaimpl;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

import org.apache.log4j.Logger;
import org.apache.log4j.Level;

public class PopularMoviesJavaImpl {

	public static void main(String[] args){
		Logger.getLogger("org").setLevel(Level.ERROR);
		
		SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("PopularMovies");
		JavaSparkContext sc = new JavaSparkContext(conf);
		
		JavaRDD<String> file = sc.textFile("resources/u.data");
		JavaPairRDD<Integer, Integer> mapped = file.mapToPair(
				new PairFunction<String, Integer, Integer>(){
					public Tuple2<Integer, Integer> call(String s){
						String[] splitted = s.split("\t");
						return new Tuple2<Integer, Integer>(Integer.valueOf(splitted[1]),1);
					}
				}).reduceByKey(
						new Function2<Integer, Integer, Integer>(){
							public Integer call(Integer x, Integer y){
								return x+y;
							}
						});
		
		for(Object o : mapped.collect()){
			System.out.println(o);
		}
		
	}
	
}

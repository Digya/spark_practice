package javaimpl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.broadcast.Broadcast;

import scala.Tuple2;


public class PopularMoviesNicesJavaImpl {

	public static void main(String[] args){

		class splitt implements Function<String, String[]>{
			public String[] call(String x){
				return x.split(" ");
			}
		}

		Logger.getLogger("org").setLevel(Level.ERROR);

		SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("PopularMoviesNice");
		JavaSparkContext sc = new JavaSparkContext(conf);

		JavaRDD<String> tFile = sc.textFile("resources/u.data");
		
		File fileT = new File("resources/u.item");
		JavaRDD<String> anotherText = sc.textFile("resources/u.item");
		Map<Integer, String> loadMovies = new HashMap<Integer, String>();
		for(int i = 1; i < anotherText.count(); i++){
			String line = null;
			try {
				line = FileUtils.readLines(fileT).get(i);
			} catch (IOException e) {
				e.printStackTrace();
			}
			String[] splitted = line.toString().split("\\|");
		
			if(splitted.length > 1){
				loadMovies.put(Integer.valueOf(splitted[0]), splitted[1]);
			}
		}
		
		Tuple2<Integer, String> name = new Tuple2<Integer, String>(1, "digya");
		Broadcast<Map<Integer, String>> nameDict = sc.broadcast(loadMovies);
		System.out.println(nameDict.value());
		JavaPairRDD<Integer, Integer> mapped = tFile.mapToPair(
				new PairFunction<String, Integer, Integer>(){
					public Tuple2<Integer, Integer> call(String x){
						String[] splitted = x.split("\t");
						return new Tuple2<Integer, Integer>(Integer.valueOf(splitted[1]),1);
					}
				}).reduceByKey(
						new Function2<Integer, Integer, Integer>(){
							public Integer call(Integer x, Integer y){
								return x+y;
							}
						});
	
	
		
		JavaRDD<Tuple2<String, Integer>> mapWithBroadCast = mapped.map(
				new Function<Tuple2<Integer, Integer>, Tuple2<String, Integer>>(){
					public Tuple2<String, Integer> call(Tuple2<Integer, Integer> x){
						Tuple2<String, Integer> mapTuple = new Tuple2<String, Integer>(nameDict.getValue().get(x._2), x._1);
						return mapTuple;
					}
				});
		for(Object o : mapWithBroadCast.collect()){
			System.out.println(o);
		}
	}
}

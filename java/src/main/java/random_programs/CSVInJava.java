/**
 * Illustrates joining two csv files
 */
package javaimpl;

import java.io.StringReader;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;

import au.com.bytecode.opencsv.CSVReader;
import scala.Tuple2;

public class CSVInJava {


	public static void main(String[] args) throws Exception {

		Logger.getLogger("org").setLevel(Level.ERROR);
		SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("CSVInJava");
		JavaSparkContext sc = new JavaSparkContext(conf);


		class ParseLine implements FlatMapFunction<Tuple2<String, String>, String[]> {
			public Iterable<String[]> call(Tuple2<String, String> file) throws Exception {
				CSVReader reader = new CSVReader(new StringReader(file._2()));
				return reader.readAll();
			}
		}
		final String key = "1";

		JavaPairRDD<String, String> csvData = sc.wholeTextFiles("resources/file.csv");
		JavaRDD<String[]> keyedRDD = csvData.flatMap(new ParseLine());
		JavaRDD<String[]> result =
				keyedRDD.filter(new Function<String[], Boolean>() {
					public Boolean call(String[] input) { return input[0].equals(key); }});
		result.saveAsTextFile("csv_output");
	}
}

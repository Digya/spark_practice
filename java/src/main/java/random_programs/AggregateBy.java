package javaimpl;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import scala.Tuple2;

import java.io.Serializable;
import java.util.Arrays;

import org.apache.log4j.*;

public class AggregateBy{

	public static void main(String[] args){

		class parse implements Function<String, Tuple2<Integer, Integer>>{

			@Override
			public Tuple2<Integer, Integer> call(String v1) throws Exception {
				String[] fields = v1.split(" ");
				int key = Integer.valueOf(fields[0]);
				int value = Integer.valueOf(fields[1]);
				Tuple2<Integer, Integer> toReturn = new Tuple2<Integer, Integer>(key, value);
				return toReturn;
			}

		}

		Logger.getLogger("org").setLevel(Level.ERROR);

		SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("AggregateBy");
		JavaSparkContext sc = new JavaSparkContext(conf);

		JavaRDD<Integer> rdd = sc.parallelize(Arrays.asList(1, 2, 3));
		Function2<AvgCount, Integer, AvgCount> addAndCount = 
				new Function2<AvgCount, Integer, AvgCount>(){

			@Override
			public AvgCount call(AvgCount arg0, Integer arg1) throws Exception {
				arg0.total += arg1;
				arg0.count += 1;
				System.out.println(arg0.total + "  " + arg0.count);
				return arg0;
			}
		};

		Function2<AvgCount, AvgCount, AvgCount> combine = 
				new Function2<AvgCount, AvgCount, AvgCount>(){

			@Override
			public AvgCount call(AvgCount v1, AvgCount v2) throws Exception {
				System.out.println(v1.count + "   " + v1.total);
				System.out.println(v2.count + "   " + v2.total);
				v1.total += v2.total;
				v1.count += v2.total;
				System.out.println(v1.total + "m  "+ v1.count);
				return v1;
			}
		};
		
		AvgCount initial = new AvgCount(0, 0);
		AvgCount s = rdd.aggregate(initial, addAndCount, combine);
		System.out.println(s);
		System.out.println(s.giveAverage());

	}
}

package javaimpl;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;
import org.apache.log4j.Logger;

import java.util.List;

import org.apache.log4j.Level;

public class MostPopularSuperheroesImpl {

	public static void main(String[] args){

		class countOccurences implements PairFunction<String, Integer, Integer>{
			@Override
			public Tuple2<Integer, Integer> call(String x){
				String[] splitted = x.split(" ");
				return new Tuple2<Integer, Integer>(Integer.valueOf(splitted[0]), splitted.length-1);
			}
		}

		class idAndMovies implements PairFunction<String, Integer, String>{
			@Override
			public Tuple2<Integer, String> call(String x){
				String[] splitted = x.split(" ");
				return new Tuple2<Integer, String>(Integer.valueOf(splitted[0].trim()), splitted[1].trim());
			}

		}
		Logger.getLogger("org").setLevel(Level.ERROR);

		SparkConf conf = new SparkConf().setAppName("MostPopularSuperheroes").setMaster("local[*]");
		JavaSparkContext sc = new JavaSparkContext(conf);

		JavaRDD<String> graphs = sc.textFile("resources/Marvel-graph.txt");
		JavaRDD<String> moviesIds = sc.textFile("resources/Marvel-names.txt");

		JavaPairRDD<Integer, Integer> graphMap = graphs.mapToPair(new countOccurences());
		JavaPairRDD<Integer, String> movieIdMap = moviesIds.mapToPair(new idAndMovies());

		JavaPairRDD<Integer, Integer> flipped = graphMap.mapToPair(
				new PairFunction<Tuple2<Integer,Integer>, Integer, Integer>(){
					public Tuple2<Integer, Integer> call(Tuple2<Integer, Integer> x){
						return new Tuple2<Integer, Integer>(x._2, x._1);
					}
				}).sortByKey(false);

		Tuple2<Integer, Integer> one = flipped.first();
		System.out.println(flipped.first());
		System.out.println("lookup " + movieIdMap.lookup(one._2));
		List<String> movie = movieIdMap.lookup(one._2);
		for(String m : movie){
		System.out.println("the movie is " + m);
		}
	}
}

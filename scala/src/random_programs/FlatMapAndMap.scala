package practice

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._

object FlatMapAndMap {

  def main(args : Array[String]){
    Logger.getLogger("org").setLevel(Level.ERROR)
    
    val sc = new SparkContext("local[*]", "FlatMapAndMap")
    val lines = sc.textFile("resources/FlatMapAndMap")
    println("====== " + lines)
    val mappedLines = lines.map(x => x.concat("test"))
    println("this is before action" + mappedLines)
    for(mappedLine <- mappedLines){
      println("This is after map line")
      println("  ")
      println(mappedLine)
    }
    
    val flatMappedLines = lines.flatMap { x => x.concat("7283786547") }
    flatMappedLines.foreach(println)
  }
}

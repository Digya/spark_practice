package three

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._
import scala.io.Source
import java.nio.charset.CodingErrorAction
import scala.io.Codec

object MovieSimilarities {
  
   def loadMovieNames() : Map[Int, String] = {
     
     implicit val codec = Codec("UTF-8")
     codec.onMalformedInput(CodingErrorAction.REPLACE)
     codec.onUnmappableCharacter(CodingErrorAction.REPLACE)
     
     var movieNames : Map[Int, String] = Map()
     
     var lines = Source.fromFile("src/resources/u.item").getLines()
     
     for(line <- lines){
       var fields = line.split("|")
       
       if(fields.length > 1){
         movieNames += fields(0).toInt -> fields(1)
       }
       
     }
     return movieNames
   }
   
   
}
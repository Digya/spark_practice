package three

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._

object RatingsCounter {
  
  
  def main(args : Array[String]){
    
    Logger.getLogger("org").setLevel(Level.ERROR)
    
    val sc = new SparkContext("local[*]", "RatingsCounter")
    val lines = sc.textFile("../u.data")

    val count = lines.map(x => x.toString().split("\t")(2))
    val fullCount = count.countByValue()
    val sortedCount = fullCount.toSeq.sortBy(_._2)
    sortedCount.foreach(println)
    
    val count = lines.map(x => (x.split("\t")(2),1))
    val reduce = count.reduceByKey((x,y) => (x+y))
    val sorted = reduce.sortByKey()
    sorted.collect.foreach(println)
    
    
  }
}

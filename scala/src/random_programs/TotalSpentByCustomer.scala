package three

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._

object TotalSpentByCustomer {
  
  def extractCustomerPricePairs(line : String) = {
    val fields = line.split(",")
    val customerId = fields(0).toInt
    val amountSpent = fields(2).toFloat
    
    (customerId, amountSpent)
  }
  
  def main(args : Array[String]){
    
    Logger.getLogger("org").setLevel(Level.ERROR)
    val sc = new SparkContext("local[*]", "TotalSpentByCustomer")
    val lines = sc.textFile("resources/customer-orders.csv")
    
    val rdd = lines.map(extractCustomerPricePairs)
    
    val reduce = rdd.reduceByKey((x,y) => (x + y))
   
    reduce.collect.foreach(println)
  }
}

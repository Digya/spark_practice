package impl

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.log4j._

object TotalSpentByCustomer {
 
  def parse(line : String)={
    val fields = line.split(",")
    val customerId = fields(0).toInt
    val amountSpent = fields(2).toFloat
    (customerId, amountSpent)
  }
  
  def main(args : Array[String]){
    
    Logger.getLogger("org").setLevel(Level.ERROR)
    
    val conf = new SparkConf().setAppName("TotalSpentBy").setMaster("local[*]")
    val sc = new SparkContext(conf)
    
    val file = sc.textFile("resources/customer-orders.csv")
    val parsed = file.map(parse)
    
    val reduced = parsed.reduceByKey((x,y) => (x+y))
    val highestAmount = reduced.map(x => (x._2,x._1)).sortBy(_._1)
    
    highestAmount.collect().foreach(println)
  }
}

package three

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.log4j._
import org.apache.spark.SparkContext._

object AggregateByPractice {
 
  def parse(line : String)={
    val fields = line.split(" ");
    val key = fields(0).toString
    val value = fields(1).toInt
    (key, value)
  }
  def main(args : Array[String]){
    
    Logger.getLogger("org").setLevel(Level.ERROR)
    val conf = new SparkConf().setMaster("local[*]").setAppName("AggregateBy")
    val sc = new SparkContext(conf)
    val file = sc.textFile("resources/newNumberResource")
    val mapped = file.map(parse)
    val aggregated = mapped.aggregateByKey((0,0))(
      (acc, value) => (acc._1 + value, acc._2 + 1),
      (acc1, acc2) => (acc1._1 + acc2._1, acc1._2+acc2._2))
      
    val avg = aggregated.mapValues(x => x._1/x._2)
    avg.foreach(println)
    
    val avg_next = mapped.mapValues(x => (x, 1)).reduceByKey((x,y) => (x._1+y._1, x._2+y._2)).mapValues(x => x._1/x._2)
    println("avg_next")
    avg_next.foreach(println)
    
  }
}

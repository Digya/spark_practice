package three

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext._
import org.apache.spark._
import org.apache.log4j._
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.regression.LinearRegressionWithSGD
import org.apache.spark.mllib.optimization.SquaredL2Updater

object MyLinearRegression {
  
  def main(args : Array[String]){
    Logger.getLogger("org").setLevel(Level.ERROR);
    
    val conf = new SparkConf().setAppName("MyLinearRegression").setMaster("local[*]")
    val sc = new SparkContext(conf)
    
    val trainingLines = sc.textFile("resources/regression_train.txt")
    val testingLines = sc.textFile("resources/regression_test.txt")
    
    val trainingData = trainingLines.map(LabeledPoint.parse).cache()
    val testData = trainingLines.map(LabeledPoint.parse)
    
    val algorithm = new LinearRegressionWithSGD()
    algorithm.optimizer.setNumIterations(100)
    .setStepSize(1.0).setUpdater(new SquaredL2Updater).setRegParam(0.01)
    
    val model = algorithm.run(trainingData)
    val predictions = model.predict(testData.map(x => x.features))
    
    val predictionsAndLabel = predictions.zip(testData.map(x => x.label)) 
    
    predictionsAndLabel.foreach(println)
    
    
    
    
  }
}

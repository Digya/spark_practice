package practice

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._

object Temperature {
 
   def parseLine(line : String) = {
     val fields = line.split(",")
     val id = fields(0).toString()
     val typeTemperature = fields(2).toString()
     val temperature = fields(3)
   
     (id, typeTemperature, temperature)
   }
   
   def main(args : Array[String]){
     
     Logger.getLogger("org").setLevel(Level.ERROR)
     
     val sc = new SparkContext("local[*]", "Temperature")
     val lines = sc.textFile("resources/1800.csv")
     val mappedLines = lines.map(parseLine)
     println("the mappedlines are " )
     mappedLines.foreach(println)
    
     val tmin = mappedLines.filter(x => x._3 == "TMIN")
     
     println("tmin is " )
     tmin.foreach(println)
     
   }
}

package three

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._
import org.apache.spark.sql._

object SparkSQL{

case class Person(id : Integer, age : Integer, name : String)

def parse(line : String) : Person= {
		val fields = line.split(",")
				val person : Person= Person(fields(0).toInt, fields(2).toInt, fields(1).toString())
				return person
}

def main(args : Array[String]) {

	Logger.getLogger("org").setLevel(Level.ERROR)

	val spark = SparkSession.builder.appName("SparkSQL").master("local[*]").getOrCreate()
	val lines = spark.sparkContext.textFile("resources/fakefriends.csv")
	
	val rdd = lines.map(parse)
	
	import spark.implicits._
	
	val schemaRdd = rdd.toDS()
	schemaRdd.printSchema()
	
	schemaRdd.createOrReplaceTempView("p")
	
	val results = spark.sql("SELECT * FROM p WHERE age >=15")
	
	results.collect().foreach(println)

	
	
}
}

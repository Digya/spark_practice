package impl

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext._
import org.apache.spark.SparkContext
import org.apache.log4j._

object PopularMovies {
 
  def main(args : Array[String]){
   
    Logger.getLogger("org").setLevel(Level.ERROR)
    
   
    val appName = args(0).toString()
    val master = args(1)
    
    val conf = new SparkConf().setMaster(master).setAppName(appName)
    val sc = new SparkContext(conf)
    
    val file = sc.textFile("resources/u.data")
    val mapped = file.map(x => (x.split("\t")(1), 1))
    
    val reduced = mapped.reduceByKey((x,y) => (x+y))
    val sortedPrint = reduced.map(x => (x._2, x._1)).sortByKey().foreach(println)
    
  }
}

package practice

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._
import scala.math.min

object MinTemperaturesPractice {

	def parseLine(line : String)= {
			val fields = line.split(",")
					val tempId = fields(0)
					val tempType = fields(2)
					val temperature = fields(3).toFloat * 0.1f * (9.0f/5.0f) + 32.0f

					(tempId, tempType, temperature)
	}

	def main(args : Array[String]) {

		Logger.getLogger("org").setLevel(Level.ERROR)
		val sc = new SparkContext("local[*]", "MinTemperatures")
		val lines = sc.textFile("src/resources/1800.csv")
		val mappedLines = lines.map(parseLine)
		val filteredLines = mappedLines.filter(x => x._2 == "TMIN")
		val newMapping = filteredLines.map(x => (x._1, x._3.toFloat))
		val reduce = newMapping.reduceByKey((x,y) => min(x,y)) 
		val results = reduce.collect()

		for (result <- results.sorted){
			val stationId = result._1
			val temperature = result._2
			val formattedTemp = f"$temperature%.2f"
			println(s"$stationId   $formattedTemp") 
		}


	}
}

package impl

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.log4j._
import scala.math.sqrt

object MovieSimilaritiesImpl {

	type MovieRatings = (Int, Double)
			type UserRatings = (Int, (MovieRatings, MovieRatings))

			def mapAnotherWay(userRatings : UserRatings) = {
					val movieRatingOne = userRatings._2._1;
					val movieRatingTwo = userRatings._2._2;
					val movie1 = movieRatingOne._1
							val movie2 = movieRatingTwo._1
							val rating1 = movieRatingOne._2
							val rating2 = movieRatingTwo._2
							((movie1, movie2), (rating1, rating2))
	}

	def filterDuplicates(userRatings : UserRatings) : Boolean = {
			val movieRatingOne = userRatings._2._1;
			val movieRatingTwo = userRatings._2._2;
			val movie1 = movieRatingOne._1
					val movie2 = movieRatingTwo._1
					return movie1 < movie2
	}
	
	type RatingPair = (Double, Double)
			type RatingPairs = Iterable[RatingPair]
					def computeCosineSimilarity(ratingPairs : RatingPairs) : (Double, Int) = {
					  var numPairs: Int = 0
					  var sumXX: Double = 0
					  var sumYY: Double = 0
					  var sumXY: Double = 0
					  
					  for(pair <- ratingPairs){
					    var ratingX = pair._1
					    var ratingY = pair._2
					    
					    sumXX += ratingX * ratingX;
					    sumYY += ratingY * ratingY;
					    sumXY += ratingX * ratingY;
					    numPairs += 1;
					    
					  }
					  
					  var numerator:Double = sumXY;
					  var denominator = sqrt(sumXX) * sqrt(sumYY)
					  var score = numerator/denominator;
					  
					  (score, numPairs)
					  
	}

	def main(args : Array[String]){
		Logger.getLogger("org").setLevel(Level.ERROR)

		val conf = new SparkConf().setMaster("local[*]").setAppName("MovieSimilaritiesImpl")
		val sc = new SparkContext(conf)

		val file = sc.textFile("resources/u.data")

		val ratings = file.map(l => l.split("\t")).map(l => (l(0).toInt, (l(1).toInt, l(2).toDouble)))

		val joinedFirstMap = ratings.join(ratings)
		//userId => (movieId, rating), (movieId, rating)
		
		ratings.take(2).foreach(println)
		
		val filteredMap = joinedFirstMap.filter(filterDuplicates)

		val afterAnotherMap = filteredMap.map(mapAnotherWay)
		//(movie1, movie2)=>(rating1, rating2)

		val groupAllRatings = afterAnotherMap.groupByKey()
		//(movie1, movie2) => (rating1, rating2), (rating1, rating2)
		val cosineLaw = groupAllRatings.mapValues(computeCosineSimilarity).cache()
		
		cosineLaw.foreach(println)
		
		val sorted = cosineLaw.groupByKey()
		
		sorted.saveAsObjectFile("output.text");

	}
}

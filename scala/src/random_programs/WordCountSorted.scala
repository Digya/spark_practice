package practice

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._

object WordCountPractice {

	def main(args : Array[String]){

		Logger.getLogger("org").setLevel(Level.ERROR)

		val sc = new SparkContext("local[*]", "WordCountPractice")
		val lines = sc.textFile("src/resources/book.txt")
	
		val splittedByWords = lines.flatMap (x => x.split("\\W"))
		val lowerCaseWords = splittedByWords.map(x => x.toLowerCase())
		val wordCounts = lowerCaseWords.map(x => (x, 1)).reduceByKey( (x,y) => x + y )
		val sortedResult = wordCounts.map(x => (x._2, x._1)).sortByKey()

		println("the sorted results are ")
		for(result <- sortedResult){

			val words = result._2
					val count = result._1
					println(s"$words : $count")
		}
	}
}

package documentation

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.log4j._

object SparkProgrammingGuide {

	def main(args : Array[String]){

		Logger.getLogger("org").setLevel(Level.ERROR);

		val conf = new SparkConf().setMaster("local[*]").setAppName("SparkProgrammingGuide");
		val sc = new SparkContext(conf);

		val file = sc.textFile("src/resources/newResource")
		
		val broadcast = sc.broadcast(file);
	
	}
}


package impl

import org.apache.spark.SparkContext._
import org.apache.spark.SparkContext
import org.apache.log4j._
import org.apache.spark.SparkConf

object RatingsCounter {
 
  def main(args : Array[String]){
    
    Logger.getLogger("org").setLevel(Level.ERROR)
    
    val conf = new SparkConf().setMaster("local[*]").setAppName("RatingsCounter")
    val sc = new SparkContext(conf)
    
    val tFile = sc.textFile("../u.data")
    val mapped = tFile.map(x => x.split("\t")(2)).countByValue()
    mapped.foreach(println)
  }
}
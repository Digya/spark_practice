ackage practice

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._
import org.apache.spark.sql._

object SparkSQLPractice {
case class Person(id : Int, name : String, age : Int, numFriends : Int)
def parse(line : String) : Person = {

		val fields = line.split(",")

				val person : Person = Person(fields(0).toInt, fields(1).toString(), fields(2).toInt, fields(3).toInt)

				return person
}

def main(args : Array[String]){

	Logger.getLogger("org").setLevel(Level.ERROR)

	val spark = SparkSession.builder.appName("SparkSQLPractice").master("local[*]").getOrCreate()

	val lines = spark.sparkContext.textFile("src/resources/fakefriends.csv")
	
	val mappedLines = lines.map(parse)
	
	import spark.implicits._
	val mappedToDS = mappedLines.toDS()
	
	mappedToDS.printSchema()
	
	mappedToDS.createOrReplaceTempView("digya")
	
	val query = spark.sql("SELECT * FROM DIGYA WHERE AGE > 50")
	
	val results = query.collect();
	
	results.foreach(println)

  }
}

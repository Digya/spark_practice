package assignment

import org.apache.spark._
import org.apache.log4j._

object CustomerExpenditure {
 
  def parseLines(lines : String) = {
    val fields = lines.split(",")
    val customerId = fields(0).toInt
    val amountSpent = fields(2).toDouble
    
    (customerId, amountSpent)
  }
  
  def main(args : Array[String]){
    
    Logger.getLogger("org").setLevel(Level.ERROR)
    
    val sc = new SparkContext("local[*]", "CustomerExpenditure")
    val lines = sc.textFile("customer-orders.csv")
    
    val mappedData = lines.map(parseLines)
    
    val amountMapping = mappedData.reduceByKey((x,y) => (x+y))
    
    val results = amountMapping.sortBy(_._1)collect()
    
    for(result <- results){
      val customerId = result._1
      val amountSpent = result._2
      println(s"customerId : $customerId, amountSpent : $amountSpent")
    }
  }
}

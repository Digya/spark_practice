package impl

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext._
import org.apache.spark.SparkContext
import org.apache.log4j._
import scala.io.Source
import java.nio.charset.CodingErrorAction
import scala.io.Codec

object PopularMoviesNicerImpl {

	def loadMovieNames(): Map[Int, String] = {
	
	  implicit val codec = Codec("UTF-8")
	  codec.onMalformedInput(CodingErrorAction.REPLACE)
	  codec.onUnmappableCharacter(CodingErrorAction.REPLACE)
	  
	  var movieNames : Map[Int, String] = Map()
	  val lines = Source.fromFile("resources/u.item").getLines()
	  
	  for(line <- lines){
	    var field = line.split('|')
	    var id = field(0).toInt
	    var movieName = field(1).toString()
	    movieNames += (id -> movieName)
	  }
	  
	  return movieNames
	  
	}
	  def main(args : Array[String]){
	    
	    Logger.getLogger("org").setLevel(Level.ERROR)
	    
	    val conf = new SparkConf().setMaster("local[*]").setAppName("PopularMoviesNicerImpl")
	    val sc = new SparkContext(conf)
	    val nameDict = sc.broadcast(loadMovieNames)
	    
	    val tFile = sc.textFile("resources/u.data")
	    val mapped = tFile.map(x => (x.split("\t")(1).toInt,1))
	    val reduced = mapped.reduceByKey((x,y) => (x+y)).map(x => (x._2, x._1))
	    val sorted = reduced.sortByKey()
	    val valued = sorted.map(x => (nameDict.value(x._2), x._1))
	    valued.collect.foreach(println)
	  }
	}

package spark

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._
import org.apache.spark.sql._

object MyDataFrames {
  
  case class Person(id : Int, name : String, age : Int, numFriends : Int)
  
  def mapper(line : String) : Person = {
    val fields = line.split(',')
    val person:Person = Person(fields(0).toInt, fields(1), fields(2).toInt, fields(3).toInt)
    return person   
  }
  
  def main(args : Array[String]){
  
    Logger.getLogger("org").setLevel(Level.ERROR)
    
    val spark = SparkSession.builder.appName("MyDataFrames").master("local[*]").getOrCreate()
    
    import spark.implicits._
    val lines = spark.sparkContext.textFile("resources/fakefriends.csv");
    val people = lines.map(mapper).toDS().cache()
    
    people.printSchema()
    
    people.select(people("name")).show()
   // people.select(people, cols)
    
  }
}

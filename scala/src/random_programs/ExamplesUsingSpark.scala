package documentation

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.streaming._
import org.apache.spark.streaming.StreamingContext._
import org.apache.spark.api.java.function._
import org.apache.spark.streaming._
import org.apache.spark.streaming.api._
import org.apache.log4j.Logger
import org.apache.log4j.Level

object ExamplesUsingSpark {
 
  def main(args: Array[String]){
   
    Logger.getLogger("org").setLevel(Level.ERROR)
    val ssc = new StreamingContext("local[2]", "ExamplesUsingSpark", Seconds(5))
    val lines = ssc.socketTextStream("localhost", 9999)                                   
    val words = lines.flatMap(x => x.split(" "))
    val mapped = words.map(x => (x,1)).reduceByKey((x,y) =>(x+y))
    mapped.print()
    ssc.start()
    ssc.awaitTermination()    
        
  }
}
package three

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._

object WordCount {
  
  def main(args : Array[String]){
    
    Logger.getLogger("org").setLevel(Level.ERROR)
    
    val sc = new SparkContext("local[*]", "WordCount")
    val lines = sc.textFile("src/resources/book.txt")
    val splitted = lines.flatMap(x => x.split("\\W+"))
    val sortedCount = splitted.countByValue().map(x => (x._2, x._1)).toSeq.sortBy(_._1).take(10).foreach(println)
    
  }
}
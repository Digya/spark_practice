package impl

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.log4j._
import scala.math.max

object MostPopularSuperheroImpl {
 
  def countConnections(line : String)= {
    val fields = line.split("\\s+")
    (fields(0).toInt, fields.length-1)
  }
  
  def idWithMovieName(line : String) : Option[(Int, String)]={
    val fields = line.split("\"")
    if(fields.length > 1){
      return Some(fields(0).trim.toInt, fields(1).toString())
    } else{
      return None
    }
  }
  
  def main(args : Array[String])={
    Logger.getLogger("org").setLevel(Level.ERROR)
    
    val conf = new SparkConf().setAppName("MostPopularSuperheroImpl").setMaster("local[*]")
    val sc = new SparkContext(conf)
    
    val fileOne = sc.textFile("resources/Marvel-names.txt")
    val fileTwo = sc.textFile("resources/Marvel-graph.txt")
    
    val connections = fileTwo.map(countConnections)
    val movieNames = fileOne.flatMap(idWithMovieName)
    
    val norepeat = connections.reduceByKey((x,y) => x+y).map(x => (x._2, x._1))
    norepeat.foreach(println)
    
    val findMax = norepeat.max()
    println("the max is " + findMax._1)
    val findMovie = movieNames.lookup(findMax._2)
    println("the most popular movies is " + findMovie(0))
  }
}

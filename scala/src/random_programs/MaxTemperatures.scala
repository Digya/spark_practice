package assignment

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._
import scala.math.max

object MaxTemperaturesPractice {
 
  def parseLine(line : String) = {
    val fields = line.split(",")
    val stationId = fields(0)
    val temperatureType = fields(2)
    val temperature = fields(3).toFloat *0.1f *(9.0f/5.0f) + 32.0f
    
    (stationId, temperatureType, temperature)
  }
  
  def main(args : Array[String]) {
    
     Logger.getLogger("org").setLevel(Level.ERROR)
     
     val sc = new SparkContext("local[*]", "MaxTemperatures")
     val lines = sc.textFile("src/resources/1800.csv")
     val mappedLines = lines.map(parseLine)
   
     val filteredValues = mappedLines.filter(x => x._2 == "TMAX")
     val newMapping = filteredValues.map(x => (x._1, x._3.toFloat))
     
     val reduceMap = newMapping.reduceByKey((x,y) => max(x,y))
     val results = reduceMap.collect()
     
     for(result <- results.sorted) {
       val stationId = result._1
       val temperature = result._2
       val formattedTemperature = f"$temperature%.2f"
      
       println(s"$stationId    $formattedTemperature")
     }
     
  }
  
}

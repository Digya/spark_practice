package impl

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.log4j._

object WordCountImpl {

	def main(args : Array[String]){
		Logger.getLogger("org").setLevel(Level.ERROR)

		val conf = new SparkConf().setMaster("local[*]").setAppName("WordCountImpl")
		val sc = new SparkContext(conf)

		val txtFile = sc.textFile("/home/lt27/workspace/SparkPractice/src/resources/book.txt");
		val mapped = txtFile.flatMap(x => x.split("\\W+")).map(x => x.toLowerCase()).
				map(x => (x,1)).reduceByKey((x,y) =>(x+y)).map(x => (x._2,x._1)).sortBy(_._2).collect()

				mapped.foreach(println)
	}
}
package practice

import org.apache.spark._
import org.apache.log4j._

object PopularMovies {
  
  def main(args : Array[String]){
    
    Logger.getLogger("org").setLevel(Level.ERROR)
    
    val sc = new SparkContext("local[*]", "PopularMovies")
    val fields = sc.textFile("../u.data")
    
    val mapping = fields.map(x => (x.split("\t")(1)))
    
    val reduce = mapping.countByValue().toSeq.sortBy(_._2)
    
    val flippedResult = reduce.map(x => (x._2, x._1))

    flippedResult.foreach(println)
  }
  
}

package mlib

import org.apache.spark.mllib.linalg.{Vector, Vectors}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.rdd.RDD
import org.apache.spark.SparkConf
import org.apache.spark._
import org.apache.spark.SparkContext
import org.apache.log4j._
import org.apache.spark.mllib.linalg.distributed.{IndexedRow, IndexedRowMatrix, RowMatrix}
import org.apache.spark.mllib.linalg.distributed.{CoordinateMatrix, MatrixEntry}

object DataTypes {

	def main(args : Array[String]){
		Logger.getLogger("org").setLevel(Level.ERROR);
		val conf = new SparkConf().setAppName("DataTypes").setMaster("local[*]")
		val sc = new SparkContext(conf)


		val s1 : IndexedRow = new IndexedRow(1, Vectors.dense(1.0, 0.0, 3.0))
		val s2 : IndexedRow = new IndexedRow(2, Vectors.dense(3.0, 7.0, 6.7))

		val rows : RDD[IndexedRow]= sc.parallelize(Seq(s1,s2))
		// Create a RowMatrix from an RDD[Vector].
		val mat: IndexedRowMatrix = new IndexedRowMatrix(rows)
		val m = mat.numRows()
		print(m)

		print("for coordinate matrix")

		val s3 : MatrixEntry = new MatrixEntry(0,0,1.0);
		val s4 : MatrixEntry = new MatrixEntry(1,1,3);
		val entries: RDD[MatrixEntry] = sc.parallelize(Seq(s3,s4))
		// an RDD of matrix entries
		// Create a CoordinateMatrix from an RDD[MatrixEntry].
		val matt: CoordinateMatrix = new CoordinateMatrix(entries)

		// Get its size.
		val mm = matt.numRows()
		val n = matt.numCols()

		// Convert it to an IndexRowMatrix whose rows are sparse vectors.
		val indexedRowMatrix = mat.toIndexedRowMatrix()
		print("mm is " + mm + " n is " + n)
	}
}

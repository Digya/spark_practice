package three

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._


object FriendsByAge {
  def parse(line : String) = {
    var fields = line.split(",")
    var age = fields(2).toInt
    var numFriends = fields(3).toInt
    
    (age, numFriends)
  }
  
  
  def main(args : Array[String]){
    
    Logger.getLogger("org").setLevel(Level.ERROR)
    
    val sc = new SparkContext("local[*]", "FriendsByAge")
    val lines = sc.textFile("resources/fakefriends.csv")
    val rdd = lines.map(parse)
    val results = rdd.mapValues(x => (x,1)).reduceByKey((x,y) => (x._1 + y._1, x._2+y._2)).mapValues(x => x._1/x._2)
    results.collect()
    results.foreach(println)
    
  }
}

package quickstart

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.log4j._
import org.apache.spark.SparkContext._
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper

case class Person(name : String, lovePandas: Boolean)
object JSONScala {

	def main(args: Array[String]){

		Logger.getLogger("org").setLevel(Level.ERROR)

		val conf = new SparkConf().setMaster("local[*]").setAppName("JSONScala")
		val sc = new SparkContext(conf)
		val input = sc.textFile("resources/file.json")
		val result = input.mapPartitions(records => {
		
			val mapper = new ObjectMapper with ScalaObjectMapper
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
					mapper.registerModule(DefaultScalaModule)
					
					records.flatMap(record => {
						try {
							Some(mapper.readValue(record, classOf[Person]))
						} catch {
						case e: Exception => None
						}
					})
		}, true)
		result.filter(x =>x.lovePandas).mapPartitions(records => {
			val mapper = new ObjectMapper with ScalaObjectMapper
					mapper.registerModule(DefaultScalaModule)
					records.map(mapper.writeValueAsString(_))
		})
		.saveAsTextFile("scalaoutput")
	}
}

package quickstart

import java.io.StringReader
import java.io.StringWriter

import org.apache.spark._
//import play.api.libs.json._
//import play.api.libs.functional.syntax._
import scala.util.parsing.json.JSON
import scala.collection.JavaConversions._

import au.com.bytecode.opencsv.CSVReader
import au.com.bytecode.opencsv.CSVWriter

object BasicParseCsv {
  case class Person(name: String, favouriteAnimal: String)

  def main(args: Array[String]) {
    val sc = new SparkContext("local[*]", "BasicParseCsv")
    val input = sc.textFile("resources/file.csv")
    val result = input.map{ line =>
      val reader = new CSVReader(new StringReader(line));
      reader.readNext();
    }
    val people = result.map(x => Person(x(0), x(1)))
    val pandaLovers = people.filter(person => person.favouriteAnimal == "panda")
    pandaLovers.map(person => List(person.name, person.favouriteAnimal).toArray).mapPartitions{people =>
      val stringWriter = new StringWriter();
      val csvWriter = new CSVWriter(stringWriter);
      csvWriter.writeAll(people.toList)
      Iterator(stringWriter.toString)
    }.saveAsTextFile("csvoutput")
    
    pandaLovers.foreach(println)
    
  }
}

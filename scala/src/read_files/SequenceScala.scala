package quickstart

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.hadoop.io.{IntWritable, Text}


object BasicLoadSequenceFile {
    def main(args: Array[String]) {
 
      val sc = new SparkContext("local[*]", "SequenceFile", System.getenv("SPARK_HOME"))
      val data = sc.sequenceFile("sequenceFile", classOf[Text], classOf[IntWritable]).map{case (x, y) =>
        (x.toString, y.get())}
      val data = sc.sequenceFile[Text,IntWritable]("seqFile",3)
      println(data.collect().toList)
      val data = sc.parallelize(List(("Panda",3),("Kay",6)))
      data.saveAsSequenceFile("seqFile")
      
    }
}
